#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#define BB while(getchar()!='\n')
#define NOM_FITXER "cotxes.dat"
#define MAX_MATRICULA 8
#define MAX_MARCA 25
#define MAX_MODEL 30
#define ERROR_OBRIR_FITXER "Error en obrir fitxer"
#define ERROR_OBRIR_FITXER_HTML "Error en obrir fitxer HTML"
#define ERROR_ESCRIPTURA "Error de escriptura al fitxer"
#define ERROR_LECTURA_FITXER "Error de lectura"
#define MAX_CAP_HTML 1000
#define MAX_PEU_HTML 200
#define MAX_INFORME 10000
#define MAX 100

typedef struct {
    char matricula[MAX_MATRICULA+1];
    char marca[MAX_MARCA+1];
    char model[MAX_MODEL+1];
    int nPortes;
    int potencia;
    double consum;
    bool teEtiquetaECO;
} Cotxes; //estructura que representa el fitxer intern.

void entrarCotxe(Cotxes *cotxe);
void escriureCotxes(Cotxes cotxe);
void pintarMenu();
int alta(char nomFixer[]);
int consulta(char nomFitxer[]);
int generarInforme(char nomFitxer[]);

/*
Crear una estructura per salvar dades de cotxes

Matricula (cadena),
Marca (cadena),
Model (cadena),
númeroPortes (enter),
Potència (enter),
Consum (double),
TeEtiquetaECO (booleà).

***Menú***
1) Alta
2) Lllistat
3) Generar informe

0) Sortir

*************

1) a) Demana a l'usuari, per teclat, les dades d'un cotxe.
   b) Un cop introduides cal persistir-les al fitxer ("cotxes.dat")
   c) Un cop feta la persistencia de les dades cal demanar a l'usuari si vol introduir les dades d'un altre cotxe. Cas afirmatiu retorna al punt (a). Altrament retorna al menú.

2) Llista per pantalla totes les dades del fitxer i fer una pausa. En prémer qualsevol tecla retorna al menú.

3) Genera un informe HTML amb CSS (una taula) que mostri totes les dades en firefox.

0) Acaba el programa.
*/

int main()
{
    int opcio,error;

    do{
        pintarMenu();
        scanf("%d",&opcio);BB;
        switch(opcio){
            case 1:
                error=alta(NOM_FITXER);
                if(error==-1) printf(ERROR_OBRIR_FITXER);
                if(error==-2) printf(ERROR_ESCRIPTURA);
                break;
            case 2:
                error=consulta(NOM_FITXER);
                if(error==-1) printf(ERROR_OBRIR_FITXER);
                if(error==-3) printf(ERROR_LECTURA_FITXER);
                break;
            case 3:
                error=generarInforme(NOM_FITXER);
                if(error==-1) printf(ERROR_OBRIR_FITXER);
                if(error==-3) printf(ERROR_LECTURA_FITXER);
                if(error==-4) printf(ERROR_OBRIR_FITXER_HTML);
                break;
            case 0:
                error=0;
                printf("Opcio 0\n");
                break;
            default:
                system("clear || cls");//"clear" per a linux i "cls" per a Windows
                printf("\t----------------------------\n");
                printf("\t| *** Opcio Incorrecta *** |\n");
                printf("\t----------------------------\n");
                printf("Les opcions son de 0 a 3\n");
        }
        if(error!=0){
            printf("\nPrem una tecla per continuar...");
            getchar();
        }
    }while(opcio!=0);
}

void entrarCotxe(Cotxes *cotxe){
    char etiqueta;
    printf("\nIntrodueix la Matricula: ");
    scanf("%7[^\n]",cotxe->matricula);BB;
    printf("\nIntrodueix la Marca: ");
    scanf("%25[^\n]",cotxe->marca);BB;
    printf("\nIntrodueix el Model: ");
    scanf("%30[^\n]",cotxe->model);BB;
    printf("\nIntrodueix el numero de portes: ");
    scanf("%d",&cotxe->nPortes);BB;
    printf("\nIntrodueix la Potencia: ");
    scanf("%d",&cotxe->potencia);BB;
    printf("\nIntrodueix el Consum: ");
    scanf("%lf",&cotxe->consum);BB;
    do{
        printf("\nTe la etiqueta ECO (s/n)? ");
        scanf("%c",&etiqueta);BB;
    }while(etiqueta!='s' && etiqueta!='n');
    if(etiqueta=='s'){
        cotxe->teEtiquetaECO=true;
    }else{
        cotxe->teEtiquetaECO=false;
    }
}

void escriureCotxes(Cotxes cotxe){
    printf("\nLa matricula: %s", cotxe.matricula);
    printf("\nLa marca: %s",cotxe.marca);
    printf("\nEl model: %s",cotxe.model);
    printf("\nEl numero de portes: %d",cotxe.nPortes);
    printf("\nLa potencia: %d",cotxe.potencia);
    printf("\nEl consum: %.2lf",cotxe.consum);
    if(cotxe.teEtiquetaECO==true){
            printf("\nTe etiqueta ECO\n");
    }else{
        printf("\nNo te etiqueta ECO\n");
    }
}

void pintarMenu(){
    fflush(stdout);
    system("clear || cls");//"clear" per a linux i "cls" per a Windows
    printf("\n\t*** Menu ***\n\n");
    printf("\t1- Alta\n");
    printf("\t2- Llistar\n");
    printf("\t3- Generar Informe\n");
    printf("\t0- Sortir\n\n");
    printf("Tria Opcio(0-3): ");
}

int alta(char nomFixer[]){
    system("clear || cls");//"clear" per a linux i "cls" per a Windows
    printf("\t----------------\n");
    printf("\t| *** Alta *** |\n");
    printf("\t----------------\n");
    int n;
    Cotxes c1;
    FILE *f1;
    f1=fopen(nomFixer,"ab");
    if(f1==NULL) return -1;
    entrarCotxe(&c1);
    n=fwrite(&c1,sizeof(Cotxes),1,f1);
    if(n==0) return -2;
    fclose(f1);
    return 0;
}

int consulta(char nomFitxer[]){
    system("clear || cls");//"clear" per a linux i "cls" per a Windows
    printf("\t-------------------------\n");
    printf("\t| *** Llista Cotxes *** |\n");
    printf("\t-------------------------\n");
    int n;
    int i=1;
    Cotxes c1;
    FILE *f1;
    f1=fopen(nomFitxer,"rb");
    if(f1==NULL) return -1;
    while(!feof(f1)){
        n=fread(&c1,sizeof(Cotxes),1,f1);
        if(!feof(f1)){
            if(n==0) return -3;
            printf("\nCotxe %d",i);
            escriureCotxes(c1);
            i++;
        }
    }
    fclose(f1);
    printf("\nPrem una tecla per continuar...");getchar();
    return 0;
}

int generarInforme(char nomFitxer[]){
    char cap_html[MAX_CAP_HTML+1]="<!DOCTYPE html> \
        <html lang=\"es\"> \
          <head> \
            <meta charset=\"UTF-8\"> \
            <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"> \
            <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\"> \
            <title>Cotxes</title> \
            <link rel=\"stylesheet\" href=\"reset.css\"> \
            <link rel=\"stylesheet\" href=\"styles.css\"> \
          </head> \
          <body class=\"app\"> \
            <header class=\"header\"> \
                <h1>Informe De Cotxes</h1> \
            </header> \
            <section class=\"section\"> \
                <table class=\"table\"> \
                    <tr class=\"table-th\"> \
                        <th>Matricula</th> \
                        <th>Marca</th> \
                        <th>Model</th> \
                        <th>Numero Portes</th> \
                        <th>Potencia</th> \
                        <th>Consum</th> \
                        <th>Etiqueta ECO</th> \
                    </tr>";
    char peu_html[MAX_PEU_HTML+1]="</table> \
            </section> \
            <footer class=\"footer\"> \
                &copy; Albert Martinez Llop <br> \
                M3-UF3 \
            </footer> \
          </body> \
        </html>";
    int n;
    char informe[MAX_INFORME+1];
    informe[0]='\0';
    char textTMP[MAX];
    Cotxes c1;
    FILE *f1, *f2;
    if((f1=fopen("informe.html","w"))==NULL){
        return -4;
    }

    if((f2=fopen(nomFitxer,"r"))==NULL){
            return -1;
    }

    strcat(informe,cap_html);

    while(!feof(f2)){
        n=fread(&c1,sizeof(Cotxes),1,f2);
        if(!feof(f2)){
            if(n==0) return -3;
            strcat(informe,"<tr><td>");
            strcat(informe,c1.matricula);
            strcat(informe,"</td>");
            strcat(informe,"<td>");
            strcat(informe,c1.marca);
            strcat(informe,"</td>");
            strcat(informe,"<td>");
            strcat(informe,c1.model);
            strcat(informe,"</td>");
            sprintf(textTMP,"%d",c1.nPortes);
            strcat(informe,"<td>");
            strcat(informe,textTMP);
            strcat(informe,"</td>");
            sprintf(textTMP,"%d",c1.potencia);
            strcat(informe,"<td>");
            strcat(informe,textTMP);
            strcat(informe,"</td>");
            sprintf(textTMP,"%.2lf",c1.consum);
            strcat(informe,"<td>");
            strcat(informe,textTMP);
            strcat(informe,"</td>");
            if(c1.teEtiquetaECO) strcpy(textTMP,"<img src=\"check.png\">");
            else strcpy(textTMP,"<img src=\"x.png\">");
            strcat(informe,"<td>");
            strcat(informe,textTMP);
            strcat(informe,"</td>");
            strcat(informe,"</tr>");
        }
    }
    strcat(informe,peu_html);
    fputs(informe,f1);
    fclose(f1);
    fclose(f2);
    system("clear || cls");//"clear" per a linux i "cls" per a Windows
    printf("\t-------------------\n");
    printf("\t| *** Informe *** |\n");
    printf("\t-------------------\n");
    system("firefox informe.html");
    printf("\nInforme generat correctament");
    printf("\nPrem una tecla per continuar...");getchar();
    return 0;
}
