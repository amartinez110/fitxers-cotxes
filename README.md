# Fitxers Cotxes

Es un programa on podem donar d'alta a diversos cotxes. Despres podem decidir si mostrar-ho per el terminal o que ens ho mostri en el firefox en una taula amb estils.

## Exemple Alta
![image.png](./image.png)

## Exemple Llista
![image-6.png](./image-6.png)

## Exemple Generar Informe
![image-3.png](./image-3.png)
![image-5.png](./image-5.png)
